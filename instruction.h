#pragma once
#include <iostream>
#include <cstdio>
#include <string>
#include "memory.h"
#include "exception.h"
#include "class.h"
#include "binary.h"

#define INT 2147483648

class instruction {
public:
	operation tmp;
	std::string op, rr;
	int r, r1, r2;
	long long result, _result;
	int step;
	bool work;

	~instruction() {}

	void instruction_fetch(const Memory &Ram, const int &go) {
		tmp = Ram.op[go]; work = true;
	}

	void instruction_decode(CPU &cpu, Memory &Ram) {
		work = true;
		op = tmp.op;
		r1 = tmp.r1;
		r = tmp.r;
		if (op == "add") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "sub") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "mul") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			if (tmp.flag_dest) { r = tmp.r; cpu.vis[r] = true; }
			else { cpu.vis[32] = cpu.vis[33] = true; }
		}
		if (op == "div") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			if (tmp.flag_dest) { r = tmp.r; cpu.vis[r] = true; }
			else { cpu.vis[32] = cpu.vis[33] = true; }
		}
		if (op == "xor") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "neg") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "rem") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "li") {
			r1 = tmp.r1; r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "seq") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "sge") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "sgt") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "sle") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "slt") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "sne") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "b") {
			r = tmp.r;
		}
		if (op == "beq") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r;
		}
		if (op == "bne") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r;
		}
		if (op == "bge") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r;
		}
		if (op == "ble") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r;
		}
		if (op == "bgt") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r;
		}
		if (op == "blt") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			if (tmp.flag_imm) r2 = tmp.r2;
			else { if (cpu.vis[tmp.r2]) { work = false; return; } r2 = cpu.reg[tmp.r2]; }
			r = tmp.r;
		}
		if (op == "beqz") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r2 = 0; r = tmp.r;
		}
		if (op == "bnez") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r2 = 0; r = tmp.r;
		}
		if (op == "blez") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r2 = 0; r = tmp.r;
		}
		if (op == "bgez") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r2 = 0; r = tmp.r;
		}
		if (op == "bgtz") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r2 = 0; r = tmp.r;
		}
		if (op == "bltz") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r2 = 0; r = tmp.r;
		}
		if (op == "j") {
			r = tmp.r;
		}
		if (op == "jr") {
			if (cpu.vis[tmp.r]) { work = false; return; } r = cpu.reg[tmp.r];
		}
		if (op == "jal") {
			r = tmp.r;
		}
		if (op == "jalr") {
			if (cpu.vis[tmp.r]) { work = false; return; } r = cpu.reg[tmp.r];
		}
		if (op == "la") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "lb") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "lh") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "lw") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "sb") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			if (cpu.vis[tmp.r]) { work = false; return; } r = cpu.reg[tmp.r];
		}
		if (op == "sh") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			if (cpu.vis[tmp.r]) { work = false; return; } r = cpu.reg[tmp.r];
		}
		if (op == "sw") {
			if (tmp.flag_imm) { r1 = tmp.r1; r2 = 0; }
			else { if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1]; r2 = tmp.r2; }
			if (cpu.vis[tmp.r]) { work = false; return; } r = cpu.reg[tmp.r];
		}
		if (op == "move") {
			if (cpu.vis[tmp.r1]) { work = false; return; } r1 = cpu.reg[tmp.r1];
			r = tmp.r; cpu.vis[r] = true;
		}
		if (op == "syscall") {
			if (cpu.vis[2]) { work = false; return; }
			else {
				r = cpu.reg[2];
				int p;
				switch (r) {
				case 1:
					if (cpu.vis[4]) { work = false; return; }
					r1 = cpu.reg[4];
					break;
				case 4:
					if (cpu.vis[4]) { work = false; return; }
					p = cpu.reg[4]; rr = "";
					while (Ram.mem[p] != '\0') { rr = rr + Ram.mem[p]; ++p; }
					break;
				case 5:
					cpu.vis[2] = true; break;
				case 8:
					if (cpu.vis[4] || cpu.vis[5]) { work = false; return; }
					r1 = cpu.reg[4]; r2 = cpu.reg[5]; break;
				case 9:
					if (cpu.vis[4]) { work = false; return; }
					r1 = cpu.reg[4]; cpu.vis[2] = true; break;
				case 17:
					if (cpu.vis[4]) { work = false; return; }
					r1 = cpu.reg[4]; break;
				}
			}
		}
	}

	void excution(int &flag, int &Tmp) {
		work = true;
		if (op == "add") result = r1 + r2;
		if (op == "sub") result = r1 - r2;
		if (op == "mul") result = r1 * r2;
		if (op == "div") { result = r1 / r2; _result = r1 % r2; }
		if (op == "xor") result = r1 ^ r2;
		if (op == "neg") result = -r1;
		if (op == "rem") result = r1 % r2;
		if (op == "li") result = r1;
		if (op == "seq") result = (r1 == r2);
		if (op == "sge") result = (r1 >= r2);
		if (op == "sgt") result = (r1 > r2);
		if (op == "sle") result = (r1 <= r2);
		if (op == "slt") result = (r1 < r2);
		if (op == "sne") result = (r1 != r2);
		if (op == "beq") result = (r1 == r2);
		if (op == "bge") result = (r1 >= r2);
		if (op == "bgt") result = (r1 > r2);
		if (op == "ble") result = (r1 <= r2);
		if (op == "blt") result = (r1 < r2);
		if (op == "bne") result = (r1 != r2);
		if (op == "beqz") result = (r1 == r2);
		if (op == "bgez") result = (r1 >= r2);
		if (op == "bgtz") result = (r1 > r2);
		if (op == "blez") result = (r1 <= r2);
		if (op == "bltz") result = (r1 < r2);
		if (op == "bnez") result = (r1 != r2);
		if (op == "la") result = r1 + r2;
		if (op == "lb") result = r1 + r2;
		if (op == "lh") result = r1 + r2;
		if (op == "lw") result = r1 + r2;
		if (op == "sb") result = r1 + r2;
		if (op == "sh") result = r1 + r2;
		if (op == "sw") result = r1 + r2;
		if (op == "move") result = r1;
		if (op == "syscall") {
			switch (r) {
			case 1:
				std::cout << r1; break;
			case 4:
				std::cout << rr; break;
			case 5:
				std::cin >> r1; break;
			case 8:
				std::cin >> rr; break;
			case 10:
				flag = 1; break;
			case 17:
				flag = 2; Tmp = r1; break;
			}
		}
	}

	void memory_access(Memory &Ram) {
		std::string ss = "";
		binary bin;
		if (op == "lb") {
			for (int i = 0; i < 1; ++i) ss += Ram.mem[i + result];
			result = bin.getint(ss, 1);
		}
		if (op == "lh") {
			for (int i = 0; i < 2; ++i) ss += Ram.mem[i + result];
			result = bin.getint(ss, 2);
		}
		if (op == "lw") {
			for (int i = 0; i < 4; ++i) ss += Ram.mem[i + result];
			result = bin.getint(ss, 4);
		}
		if (op == "sb") {
			rr = bin.getstring(r, 1);
			for (int i = 0; i < 1; ++i) Ram.mem[i + result] = rr[i];
		}
		if (op == "sh") {
			rr = bin.getstring(r, 2);
			for (int i = 0; i < 2; ++i) Ram.mem[i + result] = rr[i];
		}
		if (op == "sw") {
			rr = bin.getstring(r, 4);
			for (int i = 0; i < 4; ++i) Ram.mem[i + result] = rr[i];
		}
		if (op == "syscall") {
			int l;
			switch (r) {
			case 8:
				l = rr.length();
				for (int i = 0; i < l; ++i) Ram.mem[r1 + i] = rr[i];
				break;
			case 9:
				r2 = Ram.p_data; Ram.space(r1); break;
			}
			return;
		}
	}

	void write_back(CPU &cpu, int &go) {
		work = true;
		if (op == "add") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r(); }
		if (op == "sub") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r(); }
		if (op == "mul") {
			if (tmp.flag_dest) { cpu.reg[r] = result % INT; cpu.vis[r] = false; }
			else { cpu.reg[32] = result % INT; cpu.reg[33] = result / INT; cpu.vis[32] = cpu.vis[33] = false; }
		}
		if (op == "div") {
			if (tmp.flag_dest) { cpu.reg[r] = result; cpu.vis[r] = false; }
			else { cpu.reg[32] = result; cpu.reg[33] = _result; cpu.vis[32] = cpu.vis[33] = false; }
		}
		if (op == "xor") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "neg") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "rem") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "li") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "seq") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "sge") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "sgt") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "sle") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "slt") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "sne") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "b") { go = r - 1; }
		if (op == "beq") { if (result) go = r - 1; }
		if (op == "bne") { if (result) go = r - 1; }
		if (op == "bge") { if (result) go = r - 1; }
		if (op == "ble") { if (result) go = r - 1; }
		if (op == "bgt") { if (result) go = r - 1; }
		if (op == "blt") { if (result) go = r - 1; }
		if (op == "beqz") { if (result) go = r - 1; }
		if (op == "bnez") { if (result) go = r - 1; }
		if (op == "bgez") { if (result) go = r - 1; }
		if (op == "blez") { if (result) go = r - 1; }
		if (op == "bgtz") { if (result) go = r - 1; }
		if (op == "bltz") { if (result) go = r - 1; }
		if (op == "j") { go = r - 1; }
		if (op == "jr") { go = r - 1; }
		if (op == "jal") { cpu.reg[31] = go + 1; go = r - 1; }
		if (op == "jalr") { cpu.reg[31] = go + 1; go = r - 1; }
		if (op == "la") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "lb") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "lh") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "lw") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "move") { cpu.reg[r] = result; cpu.vis[r] = false; if (r < 0 || r>34) throw wrong_r();}
		if (op == "syscall") {
			switch (r) {
			case 5:
				cpu.reg[2] = r1; cpu.vis[2] = false; break;
			case 9:
				cpu.reg[2] = r2; cpu.vis[2] = false; break;
			}
			return;
		}
	}
};