#pragma once
#include <iostream>
#include <cstdio>
#include <string>
#include "exception.h"

class operation {
public:
	std::string op;  
	int r, r1, r2;
	bool flag_imm;//true 表示立即数参与运算， false表示全是寄存器运算
	bool flag_dest;//true 表示有接收的寄存器， false表示没有
	operation(const std::string &_op = "", const int &_r = 0, const int &_r1 = 0, const int &_r2 = 0, const bool &_flag_imm = true, const bool &_flag_dest=true)
		:op(_op), r(_r), r1(_r1), r2(_r2), flag_imm(_flag_imm), flag_dest(_flag_dest){}
};

class Register {
public:
	int get_reg(const std::string x) {
		int len = x.length();
		if (x[0] != '$') throw wrong_register();
		bool flag = true;
		int ans=0;
		for (int i = 1; i < len; ++i) {
			if (x[i] >= '0' && x[i] <= '9') ans = ans * 10 + x[i] - '0';
			else { flag = false; break; }
		}
		if (flag) return ans;
		else if (x == "$zero") return 0;
		else if (x == "$at") return 1;
		else if (x == "$v0") return 2;
		else if (x == "$v1") return 3;
		else if (x == "$a0") return 4;
		else if (x == "$a1") return 5;
		else if (x == "$a2") return 6;
		else if (x == "$a3") return 7;
		else if (x == "$t0") return 8;
		else if (x == "$t1") return 9;
		else if (x == "$t2") return 10;
		else if (x == "$t3") return 11;
		else if (x == "$t4") return 12;
		else if (x == "$t5") return 13;
		else if (x == "$t6") return 14;
		else if (x == "$t7") return 15; 
		else if (x == "$s0") return 16;
		else if (x == "$s1") return 17;
		else if (x == "$s2") return 18;
		else if (x == "$s3") return 19;
		else if (x == "$s4") return 20;
		else if (x == "$s5") return 21;
		else if (x == "$s6") return 22;
		else if (x == "$s7") return 23;
		else if (x == "$t8") return 24;
		else if (x == "$t9") return 25;
		else if (x == "$k0") return 26;
		else if (x == "$k1") return 27;
		else if (x == "$gp") return 28;
		else if (x == "$sp") return 29;
		else if (x == "$s8" || x=="$fp") return 30;
		else if (x == "$ra") return 31;
		else if (x == "$lo") return 32;
		else if (x == "$hi") return 33;
		else throw wrong_register();
	}
};

class CPU {
public:
	bool vis[34];
	int reg[34];
	void initialize() {
		for (int i = 0; i < 34; ++i) {
			vis[i] = false; reg[i] = 0;
		}
		reg[29] = 4*1024*1024-1;
	}
};