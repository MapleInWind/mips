#pragma once
#include <iostream>
#include <cstdio>
#include <string>
#include <fstream>
#include "Memory.h"
#include "class.h"
#include "binary.h"
#include "exception.h"

#define N 10000
#define num_data 10000

class translation {
private:
	int getint(const std::string x) {
		int l = x.length();
		int flag = 1;
		if (x[0] == '-') flag = -1;
		int tmp = 0;
		for (int i = 0; i < l; ++i) {
			if (x[i] >= '0' && x[i] <= '9')
				tmp = tmp * 10 + x[i] - '0';
		}
		return tmp*flag;
	}

	void del_note(std::string &text) {
		int l = text.length();
		std::string s = "";
		for (int i = 0; i < l; ++i) {
			if (text[i] == '#') break;
			s = s + text[i];
		}
		text = s;
	}

	void del_space(std::string &text) {
		std::string s = "";
		int l = text.length();
		for (int i = 0; i < l; ++i) {
			if (text[i] == ' ') continue;
			s = s + text[i];
		}
		text = s;
	}

	std::string cut(const std::string &text, const int &p, const char &ch, int &q) {
		//从text第p个位置开始一直copy到ch结束，并将ch的位置记录在q中
		int l = text.length();
		std::string s;
		for (int i = p; i < l; ++i) {
			if (text[i] != ch) s += text[i];
			else {
				q = i;  return s;
			}
		}
		q = l; return s;
	}

	std::string getop(const std::string &op) {
		if (op == "addu") return "add";
		else if (op == "addiu") return "add";
		else if (op == "subu") return "sub";
		else if (op == "xoru") return "xor";
		else if (op == "remu") return "remu";
		else if (op == "mulu") return "mul";
		else if (op == "divu") return "div";
		else if (op == "negu") return "neg";
		else if (op == "mfhi" || op == "mflo") return "move";
		else return op;
	}

	void deal(std::string &text) {
		del_note(text);
		int l, r=text.length();
		for (l = 0; l < r; ++l) {
			if (text[l] != '\t' && text[l] != ' ') break;
		}
		for (; r > l; --r) {
			if (text[r] != '\t' && text[r] != ' ') break;
		}
		std::string s = "";
		bool flag = false;
		for (int i = l; i < r; ++i) {
			if (flag) {
				if (text[i] == 'n') s = s + '\n';
				else if (text[i] == 't') s = s + '\t';
				else s = s + text[i];
			}
			else if (text[i] == '\\') { flag = true; }
			else {
				flag = false;
				s = s + text[i];
			}
		}
		text = s;
	}

public:
	std::string text[N];
	int cnt_text;

	void read(const std::string &file) {
		std::ifstream fin(file);
		std::string tmp;
		cnt_text = 0;
		while (getline(fin, tmp)) {
			deal(tmp); 
			if (tmp != "") {
				text[cnt_text] = tmp; ++cnt_text;
			}
		}
		fin.close();
	}

	void data_declare(const std::string &text, Memory &Ram) {
		int q;
		binary bin;
		std::string tmp = cut(text, 1, ' ', q);
		if (tmp == "align") {
			std::string xs = cut(text, q + 1, ' ', q);
			int x = getint(xs);
			Ram.align(x);
		}
		if (tmp == "ascii") {
			std::string s = cut(text, q + 2, '"', q);
			int l = s.length();
			Ram.insert_data(s, l);
		}
		if (tmp == "asciiz") {
			std::string s = cut(text, q + 2, '"', q);
			int l = s.length();
			Ram.insert__data(s, l);
		}
		if (tmp == "byte") {
			int l = text.length();
			while (q < l) {
				std::string xs = cut(text, q + 1, ',', q);
				del_space(xs);
				int x = getint(xs);
				std::string xb = bin.getstring(x, 1);
				Ram.insert_data(xb, 1);
			}
		}
		if (tmp == "half") {
			int l = text.length();
			while (q < l) {
				std::string xs = cut(text, q + 1, ',', q);
				del_space(xs);
				int x = getint(xs);
				std::string xb = bin.getstring(x, 2);
				Ram.insert_data(xb, 2);
			}
		}
		if (tmp == "word") {
			int l = text.length();
			while (q < l) {
				std::string xs = cut(text, q + 1, ',', q);
				int x = getint(xs);
				std::string xb = bin.getstring(x, 4);
				Ram.insert_data(xb, 4);
			}
		}
		if (tmp == "space") {
			std::string xs = cut(text, q + 1, ' ', q);
			int x = getint(xs);
			Ram.space(x);
		}
	}

	void operate(const std::string &text, Memory &Ram) {
		int q;
		std::string op = cut(text, 0, ' ', q);
		std::string obj = cut(text, q + 1, '\0', q);
		del_space(obj);
		std::string a[3];
		Register reg;
		operation ans;
		ans.op = getop(op);
		if (op == "add" || op == "addu" || op == "addiu" ||
			op == "sub" || op == "subu" || op == "xor" ||
			op == "xoru" || op == "rem" || op == "remu" ||
			op == "seq" || op == "sge" || op == "sgt" ||
			op == "sle" || op == "slt" || op == "sne") {
			q = -1;
			int l = obj.length();
			int cnt = 0;
			while (q < l) {
				a[cnt] = cut(obj, q+1, ',', q); ++cnt;
			}
			ans.r = reg.get_reg(a[0]);
			ans.r1 = reg.get_reg(a[1]);
			if (a[2][0] == '$') {
				ans.flag_imm = false;
				ans.r2 = reg.get_reg(a[2]);
			}
			else {
				ans.flag_imm = true;
				ans.r2 = getint(a[2]);
			}
			ans.flag_dest = true;
		}
		else if (op == "neg" || op=="negu" || op=="li" || op=="move") {
			q = -1;
			int l = obj.length();
			int cnt = 0;
			while (q < l) {
				a[cnt] = cut(obj, q+1, ',', q); ++cnt;
			}
			ans.r = reg.get_reg(a[0]);
			if (a[1][0] == '$') {
				ans.flag_imm = false;
				ans.r1 = reg.get_reg(a[1]);
			}
			else {
				ans.flag_imm = true;
				ans.r1 = getint(a[1]);
			}
			ans.flag_dest = true;
		}
		else if (op == "mul" || op=="mulu" || op=="div" || op=="divu") {
			q = -1;
			int l = obj.length();
			int cnt = 0;
			while (q < l) {
				a[cnt] = cut(obj, q+1, ',', q); ++cnt;
			}
			if (cnt == 3) {
				ans.flag_dest = true;
				ans.r = reg.get_reg(a[0]);
				ans.r1 = reg.get_reg(a[1]); 
				if (a[2][0] == '$') {
					ans.flag_imm = false;
					ans.r2 = reg.get_reg(a[2]);
				}
				else {
					ans.flag_imm = true;
					ans.r2 = getint(a[2]);
				}
			}
			else {
				ans.flag_dest = false;
				ans.r1 = reg.get_reg(a[0]);
				if (a[1][0] == '$') {
					ans.flag_imm = false;
					ans.r2 = reg.get_reg(a[1]);
				}
				else {
					ans.flag_imm = true;
					ans.r2 = getint(a[1]);
				}
			}
		}
		else if (op == "b" || op == "j" || op == "jal") {
			if (Ram.Label.find(obj) == Ram.Label.end()) throw wrong_operation();
			ans.r = Ram.Label[obj]; ans.flag_dest = false;
		}
		else if (op == "beq" || op == "bne" || op == "bge" ||
			op == "ble" || op == "bgt" || op == "blt") {
			q = -1;
			int l = obj.length();
			int cnt = 0;
			while (q < l) {
				a[cnt] = cut(obj, q+1, ',', q); ++cnt;
			}
			ans.r1 = reg.get_reg(a[0]);
			if (Ram.Label.find(a[2]) == Ram.Label.end()) throw wrong_operation();
			ans.r = Ram.Label[a[2]];
			if (a[1][0] == '$') {
				ans.flag_imm = false;
				ans.r2 = reg.get_reg(a[1]);
			}
			else {
				ans.flag_imm = true;
				ans.r2 = getint(a[1]);
			}
			ans.flag_dest = false;
		}
		else if (op == "beqz" || op == "bnez" || op == "blez" ||
			op == "bgez" || op == "bgtz" || op == "bltz") {
			q = -1;
			int l = obj.length();
			int cnt = 0;
			while (q < l) {
				a[cnt] = cut(obj, q+1, ',', q); ++cnt;
			}
			if (Ram.Label.find(a[1]) == Ram.Label.end()) throw wrong_operation();
			ans.r = Ram.Label[a[1]];
			if (a[0][0] == '$') {
				ans.flag_imm = false;
				ans.r1 = reg.get_reg(a[0]);
			}
			else {
				throw wrong_operation();
				/*ans.flag_imm = true;
				ans.r1 = getint(a[0]);*/
			}
			ans.flag_dest = false;
		}
		else if (op == "jr" || op == "jalr" || op == "mfhi" || op == "mflo") {
			ans.r = reg.get_reg(obj); ans.flag_dest = true;
			if (op == "mfhi") ans.r1 = 33;
			if (op == "mflo") ans.r1 = 32;
		}
		else if (op == "la" || op == "lb" || op == "lh" || op == "lw" ||
			op == "sb" || op == "sh" || op == "sw") {
			q = -1;
			int l = obj.length();
			int cnt = 0;
			while (q < l) {
				a[cnt] = cut(obj, q+1, ',', q); ++cnt;
			}
			ans.r = reg.get_reg(a[0]);
			if (Ram.Data.find(a[1]) != Ram.Data.end()) {
				ans.flag_imm = true;
				ans.r1 = Ram.Data[a[1]];
			}
			else {
				ans.flag_imm = false;
				std::string tmp1 = cut(a[1], 0, '(',q);
				std::string tmp2 = cut(a[1], q + 1, ')', q);
				ans.r2 = getint(tmp1);
				ans.r1 = reg.get_reg(tmp2);
			}
			ans.flag_dest = true;
		}
		else if (op == "nop" || op == "syscall") ans.flag_dest = false;
		else throw wrong_operation();
		Ram.insert_op(ans);
	}

	void translate(Memory &Ram) {
		bool flag = true; //flag:true data；false text
		std::string Name[num_data];
		int cnt_data = 0;
		int cnt_op = 1;
		for (int i = 0; i < cnt_text; ++i) {
			int q;
			if (text[i] == ".data") { flag = true; continue; }
			if (text[i] == ".text") { flag = false; continue; }
			if (text[i][text[i].length()-1] == ':') {
				if (flag) {
					std::string name = cut(text[i], 0, ':', q);
					Ram.Data[name] = Ram.p_data;
				}
				else {
					std::string name = cut(text[i], 0, ':', q);
					Ram.Label[name] = cnt_op;
				}
			}
			else if (!flag) ++cnt_op;
			else data_declare(text[i], Ram);
		}
		for (int i = 0; i < cnt_text; ++i) {
			if (text[i] == ".data") { flag = true; continue; }
			if (text[i] == ".text") { flag = false; continue; }
			if (text[i][text[i].length() - 1] != ':' && !flag) 
				operate(text[i], Ram);
		}

	}
};