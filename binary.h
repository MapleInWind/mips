#pragma once
#include<iostream>
#include<cstdio>
#include<string>


class binary {
public:
	std::string getstring(const int &x, const int &l) {
		std::string s="    ";
		int t = x;
		for (int i = 0; i < l; ++i) {
			s[i] = (t >> (8 * i)) & 0xff;
		}
		return s;
	}

	int getint(const std::string &s, const int &l) {
		int ans=0;
		for (int i = 0; i < l; ++i) {
			ans = ans | ((s[i] & 0xff) << (8 * i));
		}
		return ans;
	}
};