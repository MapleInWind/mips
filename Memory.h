#pragma once
#include <cstdio>
#include <iostream>
#include <string>
#include <map>
#include "class.h"

class Memory {
public:
	char *mem=new char [4*1024*1024];
	operation *op=new operation [20000];
	std::map<std::string, int> Data;//将变量名与分配的内存空间起始和结束映射
	std::map<std::string, int> Label;//将label与它的第一个操作所在位置建立映射
	int p_data, p_op;//p_data表示下一次变量从p_data开始存储，p_op表示下一次操作从p_op开始存储

	Memory() { p_data = 0; p_op = 1; memset(mem, '\0', 4 * 1024 * 1024);  operation x;  for (int i = 0; i < 20000; ++i) { op[i] = x; } }
	~Memory() { delete[] mem; delete[] op; }

	int ksm(const int &n) {//求2的n次幂
		int ans = 1;
		for (int i = 0; i < n; ++i) ans *= 2;
		return ans;
	}

	void insert_data(const std::string &content, const int &length) {
		//从p_data开始写入长度为length、内容为content的数据
		for (int i = 0; i < length; ++i) {
			mem[i + p_data] = content[i];
		}
		p_data += length;
		//std::cout << "insert_data" << length + 1 << " " << p_data << "\n";
	}

	void insert__data(const std::string &content, const int &length) {
		for (int i = 0; i < length; ++i) {
			mem[i + p_data] = content[i];
		}
		mem[length + p_data] = '\0';
		p_data += length+1;
		//std::cout << "insert__data" << length+1 << " " << p_data << "\n";
	}

	void align(const int &n) {
		int t = ksm(n);
		if (p_data%t != 0)
			p_data = (p_data / t + 1)*t;
		//std::cout << "align" << n << " " << p_data << "\n";
	}

	void space(const int &n) { 
		p_data += n; //std::cout << "space" << n << " " << p_data << "\n"; 
	}

	void insert_op(const operation &x) {
		op[p_op] = x; ++p_op;
	}
};
