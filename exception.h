#pragma once
#include <iostream>
#include <cstdio>

#include <cstddef>
#include <cstring>
#include <string>

class exception {
protected:
	const std::string variant = "";
	std::string detail = "";
public:
	exception() {}
	exception(const exception &ec) : variant(ec.variant), detail(ec.detail) {}
	virtual std::string what() {
		return variant + " " + detail;
	}
};

class wrong_register : public exception {};

class wrong_label_place : public exception {};

class wrong_operation : public exception {};

class wrong_r1 : public exception {};

class wrong_r : public exception {};

