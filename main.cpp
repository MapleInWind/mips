#include <iostream>
#include <cstdio>
#include <string>
#include <fstream>
#include "Memory.h"
#include "exception.h"
#include "instruction.h"
#include "translation.h"

int max(int x, int y) {
	return x > y ? x : y;
}

int main(int argc, char* argv[]) {
	std::string file=argv[1];
	translation tra;
	Memory Ram;
	CPU cpu;
	tra.read(file);
	tra.translate(Ram);
	cpu.initialize();
	int go = Ram.Label["main"]-1;
	instruction ins[5],ii;
	bool vis[10];
	bool run=true;
	int cnt = 0, stop1 = -100, stop2=-100;
	for (int i = 0; i < 5; ++i) ins[i].step = i + 1;
	for (int i = 0; i < 5; ++i) Ram.insert_op(operation("nop",0,0,0,true,true));
	/*std::ofstream fout("out.txt");
	while (run) {
		int flag = 0, Tmp;
		++go;
		fout << "**********************************************************"<<"\n";
		fout << "step:" << Ram.op[go].op << " " << Ram.op[go].r << " " << Ram.op[go].r1 << " " << Ram.op[go].r2 << "\n";
		ii.instruction_fetch(Ram, go);
		ii.instruction_decode(cpu, Ram);
		ii.excution(flag, Tmp);
		ii.memory_access(Ram);
		ii.write_back(cpu, go);
		for (int i = 0; i < 34; ++i) fout << "|" << i << " " << cpu.reg[i] << "|"; fout << "\n";
		if (flag == 1) return 0;
		if (flag == 2) return Tmp;
	}*/
	while (run) {
		int flag = 0, Tmp;
		int stop;
		for (int i = 9; i >= 0; --i){
			stop = max(stop1, stop2);
			if (i <= stop) break;
			for (int j = 0; j < 5; ++j) {
				if (i == ins[j].step) {
					if (i == 5) { ++go; ins[j].instruction_fetch(Ram, go); if (ins[j].work == false) stop1 = ins[j].step;/*data hazard*/ }
					if (i == 6) { ins[j].instruction_decode(cpu, Ram); if (ins[j].work == false) stop1 = ins[j].step; }
					if (i == 7) { ins[j].excution(flag, Tmp); if (ins[j].work == false) stop1 = ins[j].step; }
					if (i == 8) { ins[j].memory_access(Ram); if (ins[j].work == false) stop1 = ins[j].step; }
					if (i == 9) { ins[j].write_back(cpu, go); if (ins[j].work == false) stop1 = ins[j].step;}
				}
			}
		}
		if (flag == 1) {
			//int x;
			//std::cin >> x;
			return 0;
		}
		if (flag == 2) {
			//int x;
			//std::cin >> x;
			return Tmp;
		}
		for (int i=0; i<5; ++i)
			if (ins[i].step == 9) {
				int p = 5;
				while (vis[p]) --p;
				ins[i].step = p;
			}
		for (int i = 0; i < 5; ++i)
			if (ins[i].step > stop) ++ins[i].step;
		if ((Ram.op[go].op[0] == 'j' || Ram.op[go].op[0] == 'b' || Ram.op[go].op == "syscall")) stop2 = 5;//control hazard
		bool f;
		f = true; for (int i = 0; i < 5; ++i) if (stop1 < ins[i].step) f = false; if (f) stop1 = -100;
		f = true; for (int i = 0; i < 5; ++i) if (stop2 < ins[i].step) f = false; if (f) stop2 = -100;
		memset(vis, 0, sizeof(vis));
		for (int i = 0; i < 5; ++i) vis[ins[i].step] = 1;
	}
	return 0;
}
